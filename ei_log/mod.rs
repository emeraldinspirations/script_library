#!/usr/bin/env rust-script
//! Dependencies can be specified in the script file itself as follows:
//!
//! ```cargo
//! [dependencies]
//! chrono = "0.4.23"
//! std_io_iterators = "1.0.0"
//! ```

use chrono::prelude::*;
use std::fs::OpenOptions;
use std::io::Write;
use std_io_iterators::prelude::*;

const PARAM_AS: &str = "--as=";
const PARAM_DATE: &str = "--date=";

fn main() {
    let local: DateTime<Local> = Local::now();

    let language = std::env::args()
        .find_map(|a| {
            a.starts_with(PARAM_AS)
                .then(|| a[PARAM_AS.len()..].to_owned())
        })
        .unwrap_or_default();

    let date = std::env::args()
        .find_map(|d| {
            d.starts_with(PARAM_DATE)
                .then(|| d[PARAM_DATE.len()..].to_owned())
                .map(|v| {
                    NaiveDate::parse_from_str(&v, "%Y-%m-%d")
                        .expect("1679432555 - Unable to parse date parameter")
                })
        })
        .unwrap_or(local.naive_local().date());

    let pipe_in =
        PipeInIterator::try_new().expect("1679429943 - Unable to open STDIN");

    let year = date.format("%Y").to_string();
    let week = date.format("%U").to_string();

    let file_path =
        format!("/home/emeraldinspirations/pim/{year}/week_{week}/index.md");

    let mut f = OpenOptions::new()
        .write(true)
        .append(true)
        .open(file_path.clone())
        .expect("1679430912 - Unable to open log file");

    let std_in_iter = pipe_in.map(|v| v.as_str().to_owned());

    let b = [
        String::new(),
        format!("## {}", date.format("%Y-%m-%d - %A")),
        String::new(),
        format!("{}", local.format("%+")),
        String::new(),
        format!("```{language}"),
    ]
    .into_iter()
    .chain(std_in_iter)
    .chain(["```".to_owned()].into_iter())
    .collect::<Vec<String>>()
    .join("\n");

    let line_count = b.lines().count();

    write!(f, "{b}").expect("1679433425 - Unable to write to log file");

    println!("Wrote {line_count} lines to file {file_path}");
}
