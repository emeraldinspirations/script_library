#!/usr/bin/env rust-script
//! Dependencies can be specified in the script file itself as follows:
//!
//! ```cargo
//! [dependencies]
//! chrono = "0.4.23"
//! ```

use chrono::prelude::*;
use chrono::Duration;

fn main() {
    let local: DateTime<Local> = Local::now();
    let sunday = local
        - Duration::days(local.weekday().number_from_sunday() as i64)
        + Duration::days(1);

    for x in 0..7 {
        let v = sunday + Duration::days(x);
        println!("## {}", v.format("%Y-%m-%d - %A"));
    }
}
