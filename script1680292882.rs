#!/usr/bin/env rust-script
//! Dependencies can be specified in the script file itself as follows:
//!
//! ```cargo
//! [dependencies]
//! std_io_iterators = "1.0.0"
//! ```

// cli-clipboard = "0.4.0"
// use cli_clipboard::{ClipboardContext, ClipboardProvider};
// use std_io_iterators::prelude::PipeInIterator;

fn main() {
    // let mut iter =
    // let mut std_in = PipeInIterator::try_new().expect("1680293674");
    // let mut ctx = ClipboardContext::new().unwrap();

    let data = Generator::default()
        .map(|(t, v)| format!("{{\"title\":\"{t}\",\"value\":\"{v}\"}}"))
        .collect::<Vec<String>>()
        .join(",");
    print!("[{data}]");
}

#[derive(Default)]
struct Generator {
    pub day: usize,
    pub week: usize,
}

impl Iterator for Generator {
    type Item = (String, String);

    fn next(&mut self) -> Option<Self::Item> {
        self.day += 1;
        if self.day > 7 {
            self.week += 1;
            self.day = 1;
        }
        if self.week > 4 {
            return None;
        }

        let d = match self.day {
            1 => "U",
            2 => "M",
            3 => "T",
            4 => "W",
            5 => "H",
            6 => "F",
            7 => "S",
            _ => unreachable!("1680293444"),
        };
        let w = self.week + 1;

        Some((format!("{d}{w}"), format!("%VAR_{d}{w}U%\\n%VAR_{d}{w}L%")))
    }
}
