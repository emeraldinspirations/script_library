#[derive(serde::Serialize, serde::Deserialize)]
pub struct AutoCopyEntry {
    pub title: String,
    pub value: String,
}
