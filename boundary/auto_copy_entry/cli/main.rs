mod auto_copy_entry;

use auto_copy_entry::AutoCopyEntry;
use std::{
    fs::File,
    io::Write,
    process::{Command, Stdio},
};

fn main() {
    let mut config_file = dirs::config_dir().expect("1680296119");
    config_file.push("auto_copy_entry");
    config_file.push("queue.json");

    let file = File::open(config_file.clone()).expect("1680296804");

    let mut data: Vec<AutoCopyEntry> =
        serde_json::from_reader(file).expect("1680297425");

    let Some(AutoCopyEntry { title, value }) = data.pop() else {
        Command::new("notify-send")
            .args(["Copy Queue Empty"])
            .output()
            .expect("1680297880");
        return ;
    };

    let mut xclip = Command::new("xclip")
        .args(["-selection", "c"])
        .stdin(Stdio::piped())
        .spawn()
        .expect("1680298401");

    let mut stdin = xclip.stdin.take().expect("1680298410");
    std::thread::spawn(move || {
        stdin.write_all(value.as_bytes()).expect("1680298617");
    });

    Command::new("notify-send")
        .args([title])
        .output()
        .expect("1680298114");

    let file = File::create(config_file).expect("1680296804");

    serde_json::to_writer(file, &data).expect("1680299010");
}
