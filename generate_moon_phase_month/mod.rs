#!/usr/bin/env rust-script
//! Dependencies can be specified in the script file itself as follows:
//!
//! ```cargo
//! [dependencies]
//! chrono = "0.4.23"
//! esbat = "0.1.0"
//! ```

use chrono::{TimeZone, Utc};
use esbat::PrincipalPhase;

fn main() {
    let start = Utc.ymd(2023, 1, 1);
    let end = Utc.ymd(2023, 12, 31);
    for p in esbat::daily_lunar_phase_iter(start..end) {
        println!("{} - {}", "", ""); //p.1.format("%Y-%b-%d"));
                                     // p.0.as_emoji()
    }
}
