use chrono::NaiveDate;
use lazy_regex::{lazy_regex, Captures, Lazy, Regex};
use ledger_parser::{
    Amount, Commodity, CommodityPosition, Posting, PostingAmount, Reality,
    Transaction, TransactionStatus,
};
use rust_decimal::Decimal;

pub mod prelude {
    pub use super::{
        parse, Error as ParseError, RequestModel as ParseRequestModel,
        ResponseModel as ParseResponseModel, Result as ParseResult,
    };
    pub use std_io_iterators::prelude::PipeInIterator;
}

const MATCH_DATE: &str = "Date";
const MATCH_TOTAL: &str = "Total";
const MATCH_MEDIUM: &str = "Medium";
const MATCH_BRANCH: &str = "Branch";
const MATCH_POS: &str = "POS";
const MATCH_TX: &str = "Transaction";
const MATCH_EMP_ID: &str = "Employee_ID";
const MATCH_EMP_NAME: &str = "Employee_Name";
const MATCH_BARCODE: &str = "Barcode";
static REGEX: Lazy<Regex> = lazy_regex!(
    r#"e-Receipt.*[Hy\-Vee|Fast \& Fresh][\s\S]*Date: (?P<Date>[A-Za-z]{3} \d{2} \d{4})[\s\S]*Total\s+\$(?P<Total>\d+.\d{2})[\s\S]*?(?P<Medium>Cash|Visa)[\s\S]*?Store: (?P<Branch>\d{4})\s*?POS: (?P<POS>\d+)\s*?Transaction: (?P<Transaction>\d+)\s*?Cashier: (?P<Employee_ID>\d+)\s*?Associate: (?P<Employee_Name>\w+)[\s\S]*?Barcode\s+(?P<Barcode>\d+)"#
);

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Error {
    NotFound,
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

pub struct RequestModel {
    pub data: String,
}

#[derive(Debug, PartialEq)]
pub struct ResponseModel {
    pub transaction: Transaction,
}

pub type Result = std::result::Result<ResponseModel, Error>;

struct Parser {
    date: NaiveDate,
    quantity: Decimal,
    medium: Option<String>,
    branch: Option<String>,
    pos: Option<String>,
    tx: Option<String>,
    emp_id: Option<String>,
    emp_name: Option<String>,
    barcode: Option<String>,
}

impl<'t> From<Captures<'t>> for Parser {
    fn from(value: Captures<'t>) -> Self {
        let date = {
            NaiveDate::parse_from_str(
                value.name(MATCH_DATE).unwrap().as_str(),
                "%b %d %Y",
            )
            .unwrap()
        };

        let quantity = {
            Decimal::from_str_exact(value.name(MATCH_TOTAL).unwrap().as_str())
                .unwrap()
        };

        let pos = value.name(MATCH_POS).map(|m| m.as_str().to_owned());
        let tx = value.name(MATCH_TX).map(|m| m.as_str().to_owned());
        let emp_id = value.name(MATCH_EMP_ID).map(|m| m.as_str().to_owned());
        let emp_name =
            value.name(MATCH_EMP_NAME).map(|m| m.as_str().to_owned());
        let barcode = value.name(MATCH_BARCODE).map(|m| m.as_str().to_owned());
        let branch = value.name(MATCH_BRANCH).map(|m| m.as_str().to_owned());
        let medium = value.name(MATCH_MEDIUM).map(|m| m.as_str().to_owned());

        Parser {
            date,
            quantity,
            medium,
            branch,
            pos,
            tx,
            emp_id,
            emp_name,
            barcode,
        }
    }
}

fn render_comment(
    pos: Option<String>,
    tx: Option<String>,
    emp_id: Option<String>,
    emp_name: Option<String>,
    barcode: Option<String>,
) -> Option<String> {
    let detail = [
        pos.map(|v| format!("pos:{v}")),
        tx.map(|v| format!("tx:{v}")),
        match (emp_id, emp_name) {
            (Some(v), None) | (None, Some(v)) => Some(format!("emp:{v}")),
            (Some(id), Some(name)) => Some(format!("emp:{id}-{name}")),
            _ => None,
        },
    ]
    .into_iter()
    .flatten()
    .collect::<Vec<String>>();

    let line_one = if detail.is_empty() {
        None
    } else {
        Some(detail.join("/"))
    };

    let line_two = barcode.map(|v| format!("b:{v}"));

    (line_one.is_some() || line_two.is_some()).then_some(
        [line_one, line_two]
            .into_iter()
            .flatten()
            .collect::<Vec<String>>()
            .join("\n"),
    )
}

impl From<Parser> for Transaction {
    fn from(
        Parser {
            date,
            quantity,
            medium,
            branch,
            pos,
            tx,
            emp_id,
            emp_name,
            barcode,
        }: Parser,
    ) -> Self {
        let mut neg = quantity;
        neg.set_sign_negative(true);

        let account = match medium {
            Some(v) if v == "Visa" => "assets:comm_am_cu:checking:debt9292",
            _ => "assets:cash",
        }
        .to_owned();

        Transaction {
            comment: render_comment(pos, tx, emp_id, emp_name, barcode),
            date,
            effective_date: None,
            status: None,
            code: None,
            description: branch
                .map_or("HyVee".to_owned(), |b| format!("HyVee #{b}")),
            postings: vec![
                Posting {
                    account: "expenses:food:dine_out".to_owned(),
                    reality: Reality::Real,
                    amount: Some(PostingAmount {
                        amount: Amount {
                            commodity: Commodity {
                                name: "USD".to_owned(),
                                position: CommodityPosition::Right,
                            },
                            quantity,
                        },
                        lot_price: None,
                        price: None,
                    }),
                    balance: None,
                    status: Some(TransactionStatus::Pending),
                    comment: None,
                },
                Posting {
                    account,
                    reality: Reality::Real,
                    amount: Some(PostingAmount {
                        amount: Amount {
                            commodity: Commodity {
                                name: "USD".to_owned(),
                                position: CommodityPosition::Right,
                            },
                            quantity: neg,
                        },
                        lot_price: None,
                        price: None,
                    }),
                    balance: None,
                    status: Some(TransactionStatus::Pending),
                    comment: None,
                },
            ],
        }
    }
}

impl From<Transaction> for ResponseModel {
    fn from(transaction: Transaction) -> Self {
        ResponseModel { transaction }
    }
}

pub fn parse(RequestModel { data }: RequestModel) -> Result {
    REGEX
        .captures(&data)
        .map(Parser::from)
        .map(Transaction::from)
        .map(ResponseModel::from)
        .ok_or(Error::NotFound)
}

#[cfg(test)]
mod test {
    use chrono::NaiveDate;
    use ledger_parser::{PostingAmount, Reality};
    use pretty_assertions::assert_eq;

    const TEST_DATA: &str = r#"Your e-Receipt detailing your recent purchase at Fast & Fresh.
deals	fast&fresh	aisles

Today's Transaction
Juniper, Thank you for your purchase.
Date: Mar 13 2023 07:06 AM


Cstore Bev Bar (1):

Product Image	HYV LEMONADE
7545029242
1 × $1.99
$1.99





Product Image
Hy-Vee Deals & Coupons
View Coupons
Sub-Total
$1.99
Net Amount
$1.99
MO State Food  1.23% on $1.99
$0.02
Kansas City MO Zoological District  0.12% on $1.99
$0.00
City of Liberty MO  3.38% on $1.99
$0.07
Clay County MO  1.12% on $1.99
$0.02
Total
$2.10

Payment Information

Cash
$2.10

Total Paid
$2.10

LIBERTY GAS
Address: 300 N 291 Hwy Liberty, US-MO 64068
Call: 816-415-3998
Store: 5384   POS: 2   Transaction: 68
Cashier: 284985   Associate: ROLLY
Loyalty Card # xxxxxxx6938
Earned Today: $0.00 per gallon
Redeemed Today: $0.00 per gallon
Total Rewards: $0.00 per gallon

Your feedback matters! Help us improve by telling us about your experience and enter for a chance to win a $500 Hy-Vee gift card.
Complete Survey
$4 Tornado & Drink Combo!
flexEngage	flexEngage	flexEngage	flexEngage	flexEngage
flexEngage
Update my preferences

© Hy-Vee, Inc. All Rights Reserved.
5820 Westown Parkway, West Des Moines, IA 50266
Contact Us ‌ ‌  | ‌ ‌  Find A Store ‌ ‌  | ‌ ‌  Careers
Barcode
0015384130323002000068

‌
View Online

‌
flexEngage"#;

    #[test]
    fn test1678773592() {
        use crate::text_to_ledger::parse_use_case::prelude::*;
        use ledger_parser::{
            Amount, Commodity, CommodityPosition, Posting, Transaction,
            TransactionStatus,
        };
        use rust_decimal::Decimal;
        // GIVEN
        let request_model = ParseRequestModel {
            data: TEST_DATA.to_owned(),
        };

        // WHEN
        let actual = parse(request_model);

        // THEN
        assert_eq!(
            actual,
            Ok(ParseResponseModel {
                transaction: Transaction {
                    comment: Some(
                        "pos:2/tx:68/emp:284985-ROLLY
b:0015384130323002000068"
                            .to_owned()
                    ),
                    date: NaiveDate::from_ymd_opt(2023, 3, 13).unwrap(),
                    effective_date: None,
                    status: None,
                    code: None,
                    description: "HyVee #5384".to_owned(),
                    postings: vec![
                        Posting {
                            account: "expenses:food:dine_out".to_owned(),
                            reality: Reality::Real,
                            amount: Some(PostingAmount {
                                amount: Amount {
                                    commodity: Commodity {
                                        name: "USD".to_owned(),
                                        position: CommodityPosition::Right,
                                    },
                                    quantity: Decimal::new(210, 2),
                                },
                                lot_price: None,
                                price: None
                            }),
                            balance: None,
                            status: Some(TransactionStatus::Pending),
                            comment: None,
                        },
                        Posting {
                            account: "assets:cash".to_owned(),
                            reality: Reality::Real,
                            amount: Some(PostingAmount {
                                amount: Amount {
                                    commodity: Commodity {
                                        name: "USD".to_owned(),
                                        position: CommodityPosition::Right,
                                    },
                                    quantity: Decimal::new(-210, 2),
                                },
                                lot_price: None,
                                price: None
                            }),
                            balance: None,
                            status: Some(TransactionStatus::Pending),
                            comment: None,
                        }
                    ],
                }
            })
        );
    }
}
