#!/usr/bin/env rust-script
//! Dependencies can be specified in the script file itself as follows:
//!
//! ```cargo
//! [dependencies]
//! emeraldinspirations_script_library = { version = "0.1.1", features = ["text_to_ledger"], git = "https://gitlab.com/emeraldinspirations/script_library.git" }
//! ```

/// Parse transaction reciept text, output in ledger file format
use emeraldinspirations_script_library::text_to_ledger::parse_use_case::prelude::*;

fn main() {
    //-> std::io::Result<()> {
    // TODO: -> Main Return
    // let app = Command::new(crate_name!())
    //     .author(crate_authors!())
    //     .about(crate_description!());
    // .color(clap::ColorChoice::Always)
    // .disable_colored_help(false);
    // .version(env!("GIT_COMMIT_DESCRIBE"));

    // app.get_matches();
    // .matches_list(todo!("Gimme VfsPath")) // TODO
    // .matches_default();

    let data = PipeInIterator::try_new()
        .expect("1678910833 - Unable to open STD_IN")
        .map(|v| v.to_string())
        .collect::<Vec<String>>()
        .join("\n");

    println!(
        "{}",
        parse(ParseRequestModel { data })
            .expect("1678911219 - Unable to parse STDIN")
            .transaction
    );

    // Ok(())
}
