#!/usr/bin/env rust-script
//! Dependencies can be specified in the script file itself as follows:
//!
//! ```cargo
//! [dependencies]
//! chrono = "0.4.23"
//! ```

mod celebration;
pub mod data_object;
mod markdown_table;

use chrono::prelude::*;
use chrono::Duration;

fn main() {
    let year = std::env::args()
        .nth(1)
        .expect("1678989055 - No Year Specified")
        .parse::<i32>()
        .expect("1678989130 - Year non-numeric");

    match year {
        y if y > 2100 => panic!("1678989202 - Year too large"),
        y if y < 1900 => panic!("1678989230 - Year too small"),
        _ => (),
    }

    let (month, generate_all) = match std::env::args().nth(2) {
        Some(v) => (
            v.parse::<u32>().expect("1678989341 - Month non-numeric"),
            false,
        ),
        None => (1, true),
    };

    match month {
        m if m > 12 => panic!("1678989553 - Month too large"),
        m if m < 1 => panic!("1678989586 - Year too small"),
        _ => (),
    }

    println!("{}", ColumnName::new(HeaderRow {}).into_csv_row());

    for m in if generate_all { 1..=12 } else { month..=month } {
        println!("{}", ColumnName::new(DataRow::new(year, m)).into_csv_row());
    }
}

struct HeaderRow;

struct DataRow {
    first_of_month: NaiveDate,
    first_day_of_week: usize,
    last_day_of_month: usize,
    current_day: usize,
    loop_week_starts: usize,
}

impl DataRow {
    pub fn new(year: i32, month: u32) -> Self {
        let first_of_month = NaiveDate::from_ymd_opt(year, month, 1)
            .expect("1678989703 - Unreachable");

        let (next_month, next_year) = match month {
            12 => (1, year + 1),
            m => (m + 1, year),
        };

        let end_of_month = NaiveDate::from_ymd_opt(next_year, next_month, 1)
            .expect("1678989703 - Unreachable")
            - Duration::days(1);

        let last_day_of_month = end_of_month.day() as usize;
        let first_day_of_week =
            first_of_month.weekday().num_days_from_sunday() as usize;

        let loop_week_starts = 35usize - first_day_of_week + 1;

        DataRow {
            first_of_month,
            first_day_of_week,
            last_day_of_month,
            current_day: 0,
            loop_week_starts,
        }
    }
}

struct ColumnName<V: TitleColumn> {
    week: usize,
    day: usize,
    variant: V,
}

impl<V: TitleColumn> ColumnName<V> {
    pub fn new(variant: V) -> Self {
        ColumnName {
            week: 1,
            day: 0,
            variant,
        }
    }

    pub fn advance(&mut self) {
        match self.day {
            6 => {
                self.week += 1;
                self.day = 0;
            }
            _ => {
                self.day += 1;
            }
        }
    }

    pub fn catch_end(&self) -> Option<()> {
        if self.week > 5 {
            return None;
        }

        Some(())
    }
}

trait TitleColumn {
    fn title_column(&self) -> String;
}

impl TitleColumn for HeaderRow {
    fn title_column(&self) -> String {
        "\"VAR_MMM_YYYY\"".to_owned()
    }
}

impl TitleColumn for DataRow {
    fn title_column(&self) -> String {
        self.first_of_month.format("%B %Y").to_string()
    }
}

impl<V: TitleColumn> ColumnName<V>
where
    Self: Iterator<Item = String>,
{
    pub fn into_csv_row(self) -> String {
        [self.variant.title_column()]
            .into_iter()
            .chain(self)
            .collect::<Vec<String>>()
            .join(",")
    }
}

impl Iterator for ColumnName<HeaderRow> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.catch_end()?;

        let result = format!(
            "\"VAR_{}{}\"",
            match self.day {
                0 => "U",
                1 => "M",
                2 => "T",
                3 => "W",
                4 => "H",
                5 => "F",
                6 => "S",
                _ => unreachable!("1678991745"),
            },
            self.week
        );

        self.advance();

        Some(result)
    }
}

impl Iterator for ColumnName<DataRow> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.catch_end()?;

        let result = format!(
            "\"{}\"",
            (|| {
                let v = &mut self.variant;

                if v.current_day == 0 {
                    if v.first_day_of_week == self.day {
                        v.current_day = 1;
                        return Some("1".to_owned());
                    }

                    if v.loop_week_starts > v.last_day_of_month {
                        return None;
                    }

                    let s = v.loop_week_starts;

                    v.loop_week_starts += 1;

                    return Some(format!("{s}"));
                }

                v.current_day += 1;

                if v.current_day > v.last_day_of_month {
                    return None;
                }

                Some(format!("{}", v.current_day))
            })()
            .unwrap_or_default()
        );

        self.advance();

        Some(result)
    }
}
