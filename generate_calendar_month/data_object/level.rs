#[derive(
    Debug, PartialEq, Eq, Clone, Copy, serde::Serialize, serde::Deserialize,
)]
pub enum Level {
    Bvm,
    Commemoration,
    Feast,
    Memorial,
    #[serde(rename = "Optional Memorial")]
    OptionalMemorial,
    Solemnity,
    Sunday,
}
