#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Day {
    month: usize,
    day: usize,
}

impl Day {
    pub fn new(month: usize, day: usize) -> Self {
        Day { month, day }
    }
}

struct StrVisitor;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum DayParseError {
    Expected5Char,
    DelimiterNotWhereExpected,
    NonNumericMonth,
    MonthOutOfRange,
    NonNumericDay,
    DayOutOfRange,
}

impl serde::Serialize for Day {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl<'de> serde::Deserialize<'de> for Day {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(StrVisitor)
    }
}

impl serde::de::Visitor<'_> for StrVisitor {
    type Value = Day;

    fn expecting(
        &self,
        formatter: &mut std::fmt::Formatter,
    ) -> std::fmt::Result {
        formatter.write_str("1681044168")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        v.parse::<Day>()
            .map_err(|_| serde::de::Error::custom("1681044239"))
    }
}

impl std::str::FromStr for Day {
    type Err = DayParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() != 5 {
            Err(DayParseError::Expected5Char)?;
        }

        if &s[2..3] != "-" {
            Err(DayParseError::DelimiterNotWhereExpected)?;
        }

        let month = s[0..2]
            .parse::<usize>()
            .map_err(|_| DayParseError::NonNumericMonth)?;

        if !(1..=12).contains(&month) {
            Err(DayParseError::MonthOutOfRange)?;
        }

        let day = s[3..5]
            .parse::<usize>()
            .map_err(|_| DayParseError::NonNumericDay)?;

        if !(1..=31).contains(&day) {
            Err(DayParseError::DayOutOfRange)?;
        }

        Ok(Day { month, day })
    }
}

impl std::fmt::Display for Day {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:02}-{:02}", self.month, self.day)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use pretty_assertions::assert_eq;

    struct Scenario<'a> {
        val: &'a str,
        expected: Result<Day, DayParseError>,
        case_id: &'a str,
    }

    struct TestResult<'a> {
        scenario: Scenario<'a>,
        actual: Result<Day, DayParseError>,
    }

    #[test]
    fn test1681045364() {
        // GIVEN
        let scenario_iter = [
            Scenario {
                val: "",
                expected: Err(DayParseError::Expected5Char),
                case_id: "1681044496",
            },
            Scenario {
                val: "12345",
                expected: Err(DayParseError::DelimiterNotWhereExpected),
                case_id: "1681044625",
            },
            Scenario {
                val: " 1-22",
                expected: Err(DayParseError::NonNumericMonth),
                case_id: "1681044731",
            },
            Scenario {
                val: "13-22",
                expected: Err(DayParseError::MonthOutOfRange),
                case_id: "1681044948",
            },
            Scenario {
                val: "00-22",
                expected: Err(DayParseError::MonthOutOfRange),
                case_id: "1681045660",
            },
            Scenario {
                val: "01- 2",
                expected: Err(DayParseError::NonNumericDay),
                case_id: "1681045751",
            },
            Scenario {
                val: "01-32",
                expected: Err(DayParseError::DayOutOfRange),
                case_id: "1681045901",
            },
            Scenario {
                val: "01-00",
                expected: Err(DayParseError::DayOutOfRange),
                case_id: "1681045989",
            },
            Scenario {
                val: "01-02",
                expected: Ok(Day::new(1, 2)),
                case_id: "1681046056",
            },
        ]
        .into_iter();

        // WHEN
        let results_iter = scenario_iter.map(|s: Scenario| TestResult {
            actual: s.val.parse::<Day>(),
            scenario: s,
        });

        // THEN
        results_iter.for_each(|r| {
            assert_eq!(r.actual, r.scenario.expected, "{}", r.scenario.case_id);
        });
    }

    #[test]
    fn test1681046164() {
        // GIVEN
        let day = Day::new(1, 2);

        // WHEN
        let actual = day.to_string();

        // THEN
        assert_eq!(actual, "01-02");
    }
}
