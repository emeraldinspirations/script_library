#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum OctaveOrFast {
    AbstinenceDay,
    Easter,
    FastDay,
    HolydayOfObligation,
    NativityOfTheLord,
}

struct StrVisitor;

impl serde::Serialize for OctaveOrFast {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(match self {
            OctaveOrFast::AbstinenceDay => "Abstinence Day",
            OctaveOrFast::Easter => "Easter",
            OctaveOrFast::FastDay => "Fast Day",
            OctaveOrFast::HolydayOfObligation => "Holy Day of Obligation",
            OctaveOrFast::NativityOfTheLord => "Nativity of the Lord",
        })
    }
}

impl<'de> serde::Deserialize<'de> for OctaveOrFast {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(StrVisitor)
    }
}

impl serde::de::Visitor<'_> for StrVisitor {
    type Value = OctaveOrFast;

    fn expecting(
        &self,
        formatter: &mut std::fmt::Formatter,
    ) -> std::fmt::Result {
        formatter.write_str("1680787008")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        match v.to_lowercase().as_str() {
            "abstinence day" => Ok(OctaveOrFast::AbstinenceDay),
            "easter" => Ok(OctaveOrFast::Easter),
            "fast day" => Ok(OctaveOrFast::FastDay),
            "holy day of obligation" => Ok(OctaveOrFast::HolydayOfObligation),
            "nativity of the lord" => Ok(OctaveOrFast::NativityOfTheLord),
            _ => Err(serde::de::Error::custom("1680786780")),
        }
    }
}
