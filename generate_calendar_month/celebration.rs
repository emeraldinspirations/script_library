use super::data_object::{Day, Level, OctaveOrFast};

#[derive(Debug, PartialEq, Eq, Clone, serde::Serialize, serde::Deserialize)]
struct Celebration {
    day: Day,
    line_1: String,
    line_2: String,
    level: Level,
    octave: Option<usize>,
    pub octave_or_fast: Option<OctaveOrFast>,
}
#[cfg(test)]
mod test {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test1680787067() {
        // GIVEN
        let string = r#"[
    {
        "octave_or_fast":"Abstinence day",
        "day":"01-02",
        "level":"Bvm",
        "line_1":"Line 1",
        "line_2":"Line 2",
        "octave":1
    },
    {
        "day":"03-04",
        "octave_or_fast":"easter",
        "level":"Commemoration",
        "line_1":"Line 1",
        "line_2":"Line 2"
    },
    {
        "day":"05-06",
        "octave_or_fast":"fast Day",
        "level":"Feast",
        "line_1":"Line 1",
        "line_2":"Line 2",
        "octave":3
    },
    {
        "day":"07-08",
        "octave_or_fast":"holy day of obligation",
        "level":"Memorial",
        "line_1":"Line 1",
        "line_2":"Line 2",
        "octave":4
    },
    {
        "day":"09-10",
        "octave_or_fast":"nativity of the lord",
        "level":"Optional Memorial",
        "line_1":"Line 1",
        "line_2":"Line 2",
        "octave":7
    },
    {
        "day":"11-12",
        "level":"Solemnity",
        "line_1":"Line 1",
        "line_2":"Line 2",
        "octave":8
    }
]"#;

        // WHEN
        let actual: Vec<Celebration> = serde_json::from_str(string).unwrap();

        // THEN
        assert_eq!(
            actual,
            vec![
                Celebration {
                    day: Day::new(1, 2),
                    line_1: "Line 1".to_string(),
                    line_2: "Line 2".to_string(),
                    level: Level::Bvm,
                    octave: Some(1),
                    octave_or_fast: Some(OctaveOrFast::AbstinenceDay),
                },
                Celebration {
                    day: Day::new(3, 4),
                    line_1: "Line 1".to_string(),
                    line_2: "Line 2".to_string(),
                    level: Level::Commemoration,
                    octave: None,
                    octave_or_fast: Some(OctaveOrFast::Easter),
                },
                Celebration {
                    day: Day::new(5, 6),
                    line_1: "Line 1".to_string(),
                    line_2: "Line 2".to_string(),
                    level: Level::Feast,
                    octave: Some(3),
                    octave_or_fast: Some(OctaveOrFast::FastDay),
                },
                Celebration {
                    day: Day::new(7, 8),
                    line_1: "Line 1".to_string(),
                    line_2: "Line 2".to_string(),
                    level: Level::Memorial,
                    octave: Some(4),
                    octave_or_fast: Some(OctaveOrFast::HolydayOfObligation),
                },
                Celebration {
                    day: Day::new(9, 10),
                    line_1: "Line 1".to_string(),
                    line_2: "Line 2".to_string(),
                    level: Level::OptionalMemorial,
                    octave: Some(7),
                    octave_or_fast: Some(OctaveOrFast::NativityOfTheLord),
                },
                Celebration {
                    day: Day::new(11, 12),
                    line_1: "Line 1".to_string(),
                    line_2: "Line 2".to_string(),
                    level: Level::Solemnity,
                    octave: Some(8),
                    octave_or_fast: None,
                },
            ]
        );
    }
}
