#[derive(Debug, PartialEq, Eq, Clone)]
pub struct MarkdownTable {
    field_name_vec: Vec<String>,
    data_vec: Vec<String>,
}

impl MarkdownTable {
    pub fn row_count(&self) -> usize {
        self.data_vec.len() / self.field_name_vec.len()
    }

    pub fn row(&self, index: usize) -> &[String] {
        self.data_vec
            .chunks(self.field_name_vec.len())
            .nth(index)
            .unwrap()
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ParseError {
    MalFormedRow,
    HeaderDividerMissing,
    MalFormedHeaderDivider,
    MoreDividerColsThanHeader,
    DataRowColumnOverCount,
    InvalidMargin,
}

fn split_row(
    row: &str,
) -> Result<impl Iterator<Item = String> + '_, ParseError> {
    let mut iter = row.split('|');

    match (iter.next(), iter.next_back()) {
        (Some(""), Some("")) => Ok(iter.map(str::trim).map(ToOwned::to_owned)),
        (Some(_), Some(_)) => Err(ParseError::InvalidMargin),
        _ => Err(ParseError::MalFormedRow),
    }
}

fn is_valid_header_divider_col(v: &str) -> bool {
    let mut iter = v.chars();
    matches!(
        (iter.next(), iter.next(), iter.next_back()),
        (Some(':' | '-'), Some('-'), Some(':' | '-'))
    ) && iter.all(|c| c == '-')
}

impl std::str::FromStr for MarkdownTable {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut row = s.split('\n');
        let mut data_vec = Vec::new();

        let field_name_vec = split_row(
            row.next()
                .expect("1680793146 - Unreachable: Always at-least 1 row"),
        )?
        .collect::<Vec<String>>();
        let col_count = field_name_vec.len();

        let hd_str = row.next().ok_or(ParseError::HeaderDividerMissing)?;
        let header_divider = split_row(hd_str)?.collect::<Vec<String>>();

        if header_divider.len() > col_count {
            return Err(ParseError::MoreDividerColsThanHeader);
        }

        if !{ header_divider.iter() }.all(|v| is_valid_header_divider_col(v)) {
            return Err(ParseError::MalFormedHeaderDivider);
        }

        for r in row {
            let original_len = data_vec.len();
            data_vec.extend(split_row(r)?);
            let parsed_len = data_vec.len() - original_len;

            if parsed_len > col_count {
                return Err(ParseError::DataRowColumnOverCount);
            }

            data_vec.extend((parsed_len..col_count).map(|_| String::new()));
        }

        Ok(MarkdownTable {
            field_name_vec,
            data_vec,
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test1680792651() {
        // GIVEN
        let v = String::new();

        // WHEN
        let actual = split_row(&v);

        // THEN
        assert_eq!(actual.err().unwrap(), ParseError::MalFormedRow);
    }

    #[test]
    fn test1680794274() {
        // GIVEN
        let v = "| Sample |".to_owned();

        // WHEN
        let actual = split_row(&v)
            .unwrap()
            .map(|v| v.trim().to_owned())
            .collect::<Vec<String>>();

        // THEN

        assert_eq!(actual, vec!["Sample".to_owned()]);
    }

    #[test]
    fn test1680796583() {
        // GIVEN
        let v = r#"| Col1 |
| | |"#;

        // WHEN
        let actual = v.parse::<MarkdownTable>();

        // THEN
        assert_eq!(actual, Err(ParseError::MoreDividerColsThanHeader),);
    }

    #[test]
    fn test1680796991() {
        struct Scenario<'a> {
            divider: &'a str,
            is_valid: bool,
        }

        // GIVEN
        let scenario_array = [
            Scenario {
                divider: "",
                is_valid: false,
            },
            Scenario {
                divider: "-",
                is_valid: false,
            },
            Scenario {
                divider: "--",
                is_valid: false,
            },
            Scenario {
                divider: ":--",
                is_valid: true,
            },
            Scenario {
                divider: "--:",
                is_valid: true,
            },
            Scenario {
                divider: ":-:",
                is_valid: true,
            },
            Scenario {
                divider: ":----------:",
                is_valid: true,
            },
        ]
        .into_iter();

        // WHEN

        // THEN
        scenario_array.for_each(|Scenario { divider, is_valid }| {
            assert_eq!(
                is_valid,
                is_valid_header_divider_col(divider),
                "Scenario: \"{divider}\""
            );
        });
    }

    #[test]
    fn test1680799910() {
        // GIVEN
        let v = r#"| Col1 | Col2 |
| --- | --- |
| Val1 | Val2 |
| Val3 |"#;

        // WHEN
        let actual = v.parse::<MarkdownTable>().unwrap();

        // THEN

        assert_eq!(
            actual,
            MarkdownTable {
                field_name_vec: vec!["Col1".to_owned(), "Col2".to_owned()],
                data_vec: vec![
                    "Val1".to_owned(),
                    "Val2".to_owned(),
                    "Val3".to_owned(),
                    String::new(),
                ],
            }
        );
    }

    #[test]
    fn test1680822176() {
        // GIVEN
        let invalid_iter = [" | Col1 | Col2 |", "| Col1 | Col2 | "].into_iter();

        // WHEN
        let actual = invalid_iter
            .map(str::parse)
            .collect::<Vec<Result<MarkdownTable, ParseError>>>();

        // THEN

        assert_eq!(
            actual,
            vec![
                Err(ParseError::InvalidMargin),
                Err(ParseError::InvalidMargin)
            ]
        );
    }

    #[test]
    fn test1680857518() {
        // GIVEN
        let table = MarkdownTable {
            data_vec: (1..=25)
                .map(|v| format!("Datum{v}"))
                .collect::<Vec<String>>(),
            field_name_vec: (1..=5)
                .map(|v| format!("Col{v}"))
                .collect::<Vec<String>>(),
        };

        // WHEN
        let row_count = table.row_count();
        let data = (0..row_count)
            .map(|i| {
                table
                    .row(i)
                    .iter()
                    .map(ToOwned::to_owned)
                    .collect::<Vec<String>>()
            })
            .collect::<Vec<Vec<String>>>();

        // THEN
        assert_eq!(
            (row_count, data),
            (
                5,
                [
                    ["Datum1", "Datum2", "Datum3", "Datum4", "Datum5"],
                    ["Datum6", "Datum7", "Datum8", "Datum9", "Datum10"],
                    ["Datum11", "Datum12", "Datum13", "Datum14", "Datum15"],
                    ["Datum16", "Datum17", "Datum18", "Datum19", "Datum20"],
                    ["Datum21", "Datum22", "Datum23", "Datum24", "Datum25"]
                ]
                .into_iter()
                .map(|d| d.into_iter().map(ToOwned::to_owned).collect())
                .collect()
            )
        );
    }
}
