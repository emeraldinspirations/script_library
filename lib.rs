#![warn(missing_docs)]
#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]

#[cfg(feature = "text_to_ledger")]
pub mod text_to_ledger;

#[cfg(feature = "week_date_headers")]
pub mod week_date_headers;

#[cfg(feature = "generate_calendar_month")]
pub mod generate_calendar_month;

#[cfg(feature = "generate_moon_phase_month")]
pub mod generate_moon_phase_month;

#[cfg(feature = "current_project")]
pub mod current_project;

#[cfg(feature = "ei_log")]
pub mod ei_log;

#[cfg(feature = "script1680292882")]
pub mod script1680292882;

// pub mod data_object;
