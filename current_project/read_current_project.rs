//! Return current directory or error if unset
//!
//! See [`read_current_project`]

pub use crate::gateway::_trait::Config as ConfigTrait;
use std::path::PathBuf;

/// Glob import for [`read_current_project`] use case
pub mod prelude {
    pub use super::{
        read_current_project, Error as ReadCurrentProjectError,
        RequestModel as ReadCurrentProjectRequestModel,
        ResponseModel as ReadCurrentProjectResponseModel,
        Result as ReadCurrentProjectResult,
    };
}

/// Parameters needed for [`read_current_project`] use case
#[derive(Debug, PartialEq, Eq)]
pub struct RequestModel<'a, G: ConfigTrait> {
    /// Which impl of [`ConfigTrait`] is used (can change gateway plug-in)
    pub gateway: &'a mut G,
}

/// Results of [`read_current_project`] use case when unsuccessful
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Error {
    /// Current project not set
    NotSet,
}

/// Results of [`read_current_project`] use case when successful
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ResponseModel {
    /// The path to the current project
    pub path: PathBuf,
}

/// Results of [`read_current_project`] use case
pub type Result = std::result::Result<ResponseModel, Error>;

impl From<ResponseModel> for Result {
    fn from(value: ResponseModel) -> Self {
        Ok(value)
    }
}

impl From<Error> for Result {
    fn from(value: Error) -> Self {
        Err(value)
    }
}

/// Return current directory or error if unset
///
/// # Example
///
/// ```
/// # use current_project_lib::gateway::buffer::Config;
/// # use current_project_lib::read_current_project::prelude::*;
/// # use std::path::PathBuf;
/// fn mock_path() -> PathBuf {
///     PathBuf::from("TestPath")
/// }
/// // GIVEN
/// let mut mock_gateway = Config{
///     current_project: Some(mock_path()),
///     ..Default::default()
/// };
/// let request_model = ReadCurrentProjectRequestModel {
///     gateway: &mut mock_gateway,
/// };
///
/// // WHEN
/// let actual = read_current_project(request_model);
///
/// // THEN
/// assert_eq!(
///     actual,
///     Ok(ReadCurrentProjectResponseModel { path: mock_path() })
/// );
/// ```
///
/// # Errors
///
/// Until the first project is set, an error is returned that there is no
/// project set
///
/// ```
/// # use current_project_lib::gateway::buffer::Config;
/// # use current_project_lib::read_current_project::prelude::*;
/// // GIVEN
/// let mut empty_gateway = Config::default();
/// let request_model = ReadCurrentProjectRequestModel {
///     gateway: &mut empty_gateway,
/// };
///
/// // WHEN
/// let actual = read_current_project(request_model);
///
/// // THEN
/// assert_eq!(actual, Err(ReadCurrentProjectError::NotSet))
/// ```
pub fn read_current_project<G: ConfigTrait>(
    RequestModel { gateway }: RequestModel<'_, G>,
) -> Result {
    match gateway.read_current_project() {
        Some(path) => ResponseModel { path }.into(),
        None => Error::NotSet.into(),
    }
}
