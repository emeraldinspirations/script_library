use current_project_lib::gateway::buffer::Config as ConfigBuffer;
use std::{fs::File, io::BufReader};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum LoadError {
    DirIndeterminable,
    DirNotFound,
    FileNotFound,
    FileNotOpenable,
    FileNotParsable,
}

pub type Result = std::result::Result<ConfigBuffer, LoadError>;

impl From<LoadError> for Result {
    fn from(value: LoadError) -> Self {
        Err(value)
    }
}

pub fn load_json_config() -> Result {
    let Some(mut config_dir) =  dirs::config_dir() else {
            return LoadError::DirIndeterminable.into();
    };

    config_dir.push("current_project");
    let mut config_path = config_dir.clone();
    config_path.push("config.json");

    if !config_dir.exists() {
        return LoadError::DirNotFound.into();
    }

    if !config_path.exists() {
        return LoadError::FileNotFound.into();
    }

    let Ok(file) = File::open(config_path.clone()) else {
        return  LoadError::FileNotOpenable.into();
    };

    let reader = BufReader::new(file);

    serde_json::from_reader(reader).map_err(|_| LoadError::FileNotParsable)
}
