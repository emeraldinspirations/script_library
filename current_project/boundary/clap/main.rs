//! Set current project, re-direct actions to specified path
//!
//! See [`current_project_lib`]

#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]

pub mod entity;
mod load_json_config;
mod sub_command;

use clap::{crate_authors, crate_description, crate_name, Command};
use entity::NotifySend;
use load_json_config::load_json_config;
use sub_command::Default as _;

fn main() {
    let app = Command::new(crate_name!())
        .author(crate_authors!())
        .about(crate_description!());
    // .color(clap::ColorChoice::Always)
    // .disable_colored_help(false)
    // .version(env!("GIT_COMMIT_DESCRIBE"));

    let mut config_gateway = match load_json_config() {
        Ok(g) => g,
        Err(e) => {
            NotifySend::default()
                .send("Current project not set / config file issue");
            panic!("{e:?}");
        }
    };

    app.get_matches().matches_default(&mut config_gateway);
}
