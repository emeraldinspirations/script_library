use std::process;

#[derive(Default)]
pub struct NotifySend {}

impl NotifySend {
    ///
    /// ```text
    /// notify-send [OPTION…] <SUMMARY> [BODY] - create a notification
    /// ```
    pub fn send(self, body: &str) {
        if let Err(e) = process::Command::new("notify-send").arg(body).spawn() {
            eprintln!("{e:?}");
        }
    }

    ///
    ///  -h, --hint=TYPE:NAME:VALUE
    ///        Specifies basic extra data to pass. Valid types are int, double, string and byte.
    pub fn hint(&mut self) -> &mut Self {
        self
    }

    ///
    /// -c, --category=TYPE[,TYPE...]
    ///        Specifies the notification category.
    pub fn category(&mut self) -> &mut Self {
        self
    }

    /// -i, --icon=ICON[,ICON...]
    ///        Specifies an icon filename or stock icon to display.
    pub fn icon(&mut self) -> &mut Self {
        self
    }

    ///
    /// -u, --urgency=LEVEL Specifies the urgency level (low, normal, critical).
    pub fn urgency(&mut self) -> &mut Self {
        self
    }

    ///
    /// -t, --expire-time=TIME
    ///        The duration, in milliseconds, for the notification to appear on screen.  (Ubuntu's
    ///        Notify OSD and GNOME Shell both ignore this parameter.)
    pub fn expire_time(&mut self) -> &mut Self {
        self
    }
}
