use crate::entity::NotifySend;
use clap::ArgMatches;
use current_project_lib::gateway::buffer::Config;
use current_project_lib::read_current_project::prelude::*;

pub trait Default {
    fn matches_default(self, gateway: &mut Config) -> Self;
}

impl Default for ArgMatches {
    fn matches_default(self, gateway: &mut Config) -> Self {
        if self.subcommand().is_some() {
            return self;
        }

        match read_current_project(ReadCurrentProjectRequestModel { gateway }) {
            Ok(ReadCurrentProjectResponseModel { path }) => {
                NotifySend::default().send(path.to_str().unwrap_or_default());
            }
            Err(ReadCurrentProjectError::NotSet) => {
                NotifySend::default().send("(Current project not set)");
            }
        }

        self
    }
}
