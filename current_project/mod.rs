//! Set current project, re-direct actions to specified path

#![warn(missing_docs)]
#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]

pub mod gateway;
pub mod read_current_project;
