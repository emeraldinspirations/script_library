//! A buffer that implements [`ConfigTrait`]
//!
//! See [`Config`]

use crate::gateway::_trait::Config as ConfigTrait;
use std::path::PathBuf;

/// A buffer that implements [`ConfigTrait`]
#[derive(
    Debug, PartialEq, Eq, Clone, Default, serde::Serialize, serde::Deserialize,
)]
pub struct Config {
    /// Path to the current project
    pub current_project: Option<PathBuf>,
}

impl ConfigTrait for Config {
    fn read_current_project(&mut self) -> Option<PathBuf> {
        self.current_project.clone()
    }
}
