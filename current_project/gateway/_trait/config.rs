use std::path::PathBuf;

/// Access to the configuration for the project
pub trait Config {
    /// Return the current project
    fn read_current_project(&mut self) -> Option<PathBuf>;
}
