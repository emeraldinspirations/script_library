//! The trait to implement for a specific data source

mod config;

pub use config::Config;
